#!/bin/sh
#set -x
EXPECTED_ARGS=1
E_BADARGS=65
DEBUG=0
ADHOC=0
IW_COMMAND=0

if [ $# -lt $EXPECTED_ARGS ]; then
  echo "Usage: `basename $0` <interface> <file ad-hoc> <DEBUG>"
  exit $E_BADARGS
fi

if [ $# -ge 3 ] && [ $3 = "DEBUG" ]; then
	DEBUG=1
fi

WLAN=$1
re='^[0-9]+$'
prefix0='          '
prefix='0'
prefix2='ESSID:"'
prefix3='Mode:'
suffix='"'
suffix2=')'

echo "$WLAN up"
ifconfig $WLAN down
iwconfig $WLAN mode managed
ifconfig $WLAN up

splash()
{
	clear
	echo "      _____    _______  _______  _____  ______    _____    _____   _______ "
	echo "     (  __ \  ( ______)(___ ___)(_   _)|  __  \  / ___ \  / ___ \ (___ ___)"
	echo "     | |  \ \ | |___      | |     | |  | |__)  )| |   | || |   | |   | |   "
	echo "     | |   | ||  ___)     | |     | |  |  __  ( | |   | || |   | |   | |   "
	echo "     | |__/ / | |_____    | |    _| |_ | |__)  )| |___| || |___| |   | |   "
	echo "     |_____/  |_______)   |_|   (_____)|______/  \_____/  \_____/    |_|   "
}

ord()
{
	LC_CYPE=C printf '%d' "'$1"
}

contains()
{
	a="$1"
	b="$2"
	if test "${a#*$b}" != "${a}";
	then
		return 0
	fi
	return 1
}

is_integer()
{
	if ! [ "$1" -eq "$1" ] 2> /dev/null
	then
		return 1
	fi
	return 0
}

read_char()
{
	old=$(stty -g)
	stty raw -echo min 1
	eval "$1=\$(dd bs=1 count=1 2>/dev/null)"
	stty $old
}

while true
do
	splash
	echo ""
	echo "Wireless device: ${WLAN}"
	echo -n "Scanning"
	while true
	do
		echo -n "."
		IW=`iwlist $1 scanning | grep 'Cell\|Frequency\|ESSID\|Mode'`
		MENU=$(echo "$IW" | grep 'Cell')
		
		options=$(echo "$MENU" | grep Cell | awk '{print $2}')
		
		ssids=""
		channels=""
		bssids=""
		detibootnet=""
		i=1
		
		for o in $options
		do
			o=${o#$prefix}
			SSID=$(echo "$IW" | grep 'ESSID' | awk NR==$o'{print $1}')
			SSID=${SSID#$prefix2}
			SSID=${SSID%$suffix}
			CHANNEL=$(echo "$IW" | grep 'Frequency' | awk NR==$o'{print $4}')
			CHANNEL=${CHANNEL%$suffix2}
			MODE=$(echo "$IW" | grep 'Mode' | awk NR==$o'{print $1}')
			MODE=${MODE#$prefix3}
			BSSID=$(echo "$IW" | grep 'Cell' | awk NR==$o'{print $5}')
			#if [ "$SSID" = "$BSSID" ]; then
			if [ "$MODE" = "Ad-Hoc" ]; then
				detibootnet="$detibootnet $i"
				i=`expr $i + 1`
				ssids="$ssids $SSID"
				channels="$channels $CHANNEL"
				bssids="$bssids $BSSID"
			fi
		done
		if [ "$detibootnet" != "" ]; then
			break
		fi
		sleep 1
	done
	echo ""
	
	echo "Cell BSSID             CHANNEL SSID"
	
	idx=1
	#echo $detibootnet
	#echo $bssids
	#echo $channels
	#echo $ssids
	for o in $detibootnet
	do
		echo $detibootnet | awk "{i = $idx; printf \"%s   \", \$i}"
		echo $bssids | awk "{i = $idx; printf \"%s \", \$i}"
		echo $channels | awk "{i = $idx; printf \"%02d \", \$i}"
		echo $ssids | awk "{i = $idx; printf \"     %s\n\", \$i}"
		idx=`expr $idx + 1`
		i=$((i+1))
	    #o=${o#$prefix}
	    #echo "$o    ${bssids[$((o-1))]} ${channels[$((o-1))]}       ${ssids[$((o-1))]}"
	done
	
	printf "Cell (Press return for rescan): "
	while true
	do
		option=0
		read_char option
		ORD=$(ord $option)
		if is_integer $option; then
			echo $option
			if [ $option -ge 1 ]; then
				if [ $option -lt ${idx} ]; then
					break
				fi
			fi
		elif [ $ORD = 13 ]; then
			break
		elif [ $option = '' ]; then
			exit 1
		fi
	done
	#option=${option#$prefix}
	
	valid=false
	for o in $detibootnet
	do
	    o=${o#$prefix}
	    if [ "$o" = "$option" ]
	    then
			valid=true
			break
	    fi
	done
	
	option=$(( option - 1 ))
	
	if [ $valid = true ]
	then
		i=0
		for s in $ssids
		do
			if [ $i = $option ]
			then
				SSID=$s
				break
			fi
			i=$(( i + 1 ))
		done
		i=0
		for c in $channels
		do
			if [ $i = $option ]
			then
				CHANNEL=$c
				FREQ=$(( CHANNEL * 5 ))
				FREQ=$(( FREQ + 2407 ))
				break
			fi
			i=$(( i + 1 ))
		done
		i=0
		for b in $bssids
		do
			if [ $i = $option ]
			then
				BSSID=$b
				break
			fi
			i=$(( i + 1 ))
		done
		
		#SSID=${ssids[$option]}
		#CHANNEL=${channels[$option]}
		#FREQ=$(( CHANNEL * 5 ))
		#FREQ=$(( FREQ + 2407 ))
		#BSSID=${bssids[$option]}
		break
	fi
done

set +x

if [ $DEBUG = 1 ]; then
	printf "Use Monitor instead of Ad-Hoc? (Y/n): "
	while true
	do
		option=0
		read_char option
		ORD=$(ord $option)
		if [ $ORD = 121 ] || [ $ORD = 89 ] || [ $ORD = 13 ]; then
			ADHOC=0
			if [ $# -gt $EXPECTED_ARGS ]; then
				echo " " > $2
			fi
			break
		elif [ $ORD = 78 ] || [ $ORD = 110 ]; then
			ADHOC=1
			if [ $# -gt $EXPECTED_ARGS ]; then
				echo "-a" > $2
			fi
			break
		elif [ $option = '' ]; then
			exit 1
		fi
	done
	echo $option
	printf "Use IWConfig instead of IW? (Y/n): "
	while true
	do
		option=0
		read_char option
		ORD=$(ord $option)
		if [ $ORD = 121 ] || [ $ORD = 89 ] || [ $ORD = 13 ]; then
			IW_COMMAND=0
			break
		elif [ $ORD = 78 ] || [ $ORD = 110 ]; then
			IW_COMMAND=1
			break
		elif [ $option = '' ]; then
			exit 1
		fi
	done
	echo $option
fi

#IWCHECK=$(iw ${WLAN} info 2>&1)
#if contains "${IWCHECK}" "command failed"; then # New Ad-hoc setup for cards not compatible with iw

if [ $ADHOC = 1 ]; then
	if [ $IW_COMMAND = 0 ]; then
		#echo "DOING STEP IWCONFIG"
		ifconfig $WLAN down
		iwconfig $WLAN mode ad-hoc
		ifconfig $WLAN up
		#iwconfig $WLAN essid off
		iwconfig $WLAN essid ${BSSID}
		iwconfig $WLAN ap ${SSID}
		iwconfig $WLAN channel $CHANNEL
		#ifconfig $WLAN up
	else # Default ad-hoc setup
		#echo "DOING STEP IW"
		ifconfig $WLAN down
		iw $WLAN set type ibss
		ifconfig $WLAN up
		iw $WLAN ibss leave
		#iw $WLAN ibss join $SSID $FREQ $BSSID basic-rates 54 mcast-rate 54
		iw $WLAN ibss join $SSID $FREQ
	fi
else
	if [ $IW_COMMAND = 0 ]; then
		#echo "DOING STEP IWCONFIG"
		ifconfig $WLAN down
		iwconfig $WLAN mode monitor
		ifconfig $WLAN up
		#iwconfig $WLAN channel $CHANNEL
		iwconfig $WLAN freq ${FREQ}M
	else # Default ad-hoc setup
		#echo "DOING STEP IW"
		ifconfig $WLAN down
		iw $WLAN set type monitor
		ifconfig $WLAN up
		iw $WLAN set freq $FREQ
	fi
fi
