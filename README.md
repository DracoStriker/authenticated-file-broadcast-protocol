# Authenticated File Broadcast Protocol
The File Broadcast Protocol (FBP) was previously developed as a part of the DETIboot system. DETIboot allows a host to broadcast an operating system image through an 802.11 wireless network to an arbitrary number of receivers. Receivers can load the image and immediately boot a Linux live session.
The initial version of FBP, which is based on Fountain Codes, had no security mechanisms.

We developed for FBP an extension that ensures a correct file distribution from the intended source to the receivers, the Authenticated FBP.

The performance evaluations have shown that, with the best operational configuration tested, the file download time is increased by less than 5%.

AFBP sends authenticators packages between codewords packets that validate future codewords. To increase efficiency, the works perform in parallel the production of codewords, authentication of codewords and transmition on the server side.

For more technical description and functioning of our implementation, refer to our paper:

https://link.springer.com/chapter/10.1007/978-3-319-18467-8_16

If you use this protocol or improve it in your research please give cite us as indicated down bellow:

    @InProceedings{10.1007/978-3-319-18467-8_16,
    author="Reis, Sim{\~a}o
    and Z{\'u}quete, Andr{\'e}
    and Faneca, Carlos
    and Vieira, Jos{\'e}",
    editor="Federrath, Hannes
    and Gollmann, Dieter",
    title="Authenticated File Broadcast Protocol",
    booktitle="ICT Systems Security and Privacy Protection",
    year="2015",
    publisher="Springer International Publishing",
    address="Cham",
    pages="237--251",
    abstract="The File Broadcast Protocol (FBP) was developed as a part of the DETIboot system. DETIboot allows a host to broadcast an operating system image through an 802.11 wireless network to an arbitrary number of receivers. Receivers can load the image and immediately boot a Linux live session. The initial version of FBP had no security mechanisms. In this paper we present an authentication protocol developed for FBP that ensures a correct file distribution from the intended source to the receivers. The performance evaluations have shown that, with the best operational configuration tested, the file download time is increased by less than 5{\%}.",
    isbn="978-3-319-18467-8"
    }
    
#### Acknowledgments

I thank André Zúquete, Cláudio Patrício and Rafael Saraiva Figueiredo for all their help in developing, debugging and refactoring the source code.
