/*
 * File:   fbp-client.cpp
 * Author: joao
 *
 * Created on April 5, 2013, 8:17 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <errno.h>

#include "file_handler.h"
#include "network_handler.h"
#include "time_handler.h"

#include "Decoder.hh"
#include "ReceiverSecurityManager.hh"

#include <sys/resource.h>
#include <sys/time.h>

using namespace std;

bool interrupt = false;

void sighandler (int sig)
{
	if (sig == SIGINT)
	{
		cout << endl << "Interupted by the user.";
		interrupt = true;
	}

	close_sock_list ();
}

void printmenu(BroadcastAuthenticator *authenticator) {
	cout << "Question the broadcast administrator about the key to be used." << endl;
	ReceiverSecurityManager::displayPublickeyHash (authenticator);
	cout << "yes    - y" << endl
		<< "no     - n" << endl
		<< "forget - f" << endl
		<< "quit   - q" << endl
		<< "Use this key (y/n/f/q)? ";
}

int selectoption(ReceiverSecurityManager &rsm, BroadcastAuthenticator *authenticator) {
	char opt;
	do
		opt = getchar ();
	while (isspace (opt));
	if (opt == 'f' || opt == 'F')
		rsm.forgetPublicKey (authenticator);
	else if (opt == 'y' || opt == 'Y')
		rsm.setPublickey (authenticator);
	else if (opt == 'q' || opt == 'Q')
		return -1;
	return 0;
}

static int stats = 1;

int if_recvfrom (char *ifname, char *filename, uint32_t *rx_frames, uint32_t *tx_frames, bool auth, bool adhoc)
{
	int result;

	int frame_len = 0;
	uint8_t*frame_buffer;
	struct fbp_header *header;
	uint8_t*data = NULL;
	uint32_t max_frame_size;
	off_t data_size = 0;

	Decoder *decoder = NULL;

	uint32_t iseq = 0, lseq = 0;
	uint32_t receptionprogress = 0;
	uint32_t nValid = 0, nInvalid = 0, nOtherInvalid = 0;
	double time_begin, time_end;

	// init socket
	if (if_init_client ( ifname, &max_frame_size, adhoc ? 0 : 1 ) != 0) {
		cout << "Error: initializing interface (" << ifname << "): " << strerror (errno) << endl;
		return -1;
    }

	frame_buffer = (uint8_t*) malloc( max_frame_size );

	ReceiverSecurityManager rsm;
	u_int32_t k;			// k from fbp header

	// Get first frame, which needs to be an authenticator
	if (auth)
	{
		cout << "\rWaiting for file...";
		cout.flush();

        while (!interrupt && !rsm.hasPublicKey ()) {
			result = if_read (frame_buffer, (uint8_t**) &header, max_frame_size);
			if (result == -1) {
				cout << "Error: reception error, configuration aborted: " << strerror (errno) << endl;
				return -1;
			}
			
			if (ReceiverSecurityManager::isAuthenticator (header) && !interrupt)
			{
				BroadcastAuthenticator *authenticator = (BroadcastAuthenticator *) header;
				// set public key
				if (rsm.validPublicKey (authenticator))
				{
					cout << "\n\rFile broadcast found." << endl;                    
					printmenu(authenticator);
					if (selectoption(rsm, authenticator) < 0) {
						return -1;
					}
				}
			}
		}
	}

	empty_sock ();

	// Wait for first codeword, reading Authenticators in the meanwhile

	while (!interrupt) {
		result = if_read (frame_buffer, (uint8_t**) &header, max_frame_size);
		if (result < 0) {
			cout << "Error: reception error, configuration aborted: " << strerror (errno) << endl;
			return -1;
		}
		if (ReceiverSecurityManager::isAuthenticator (header)) {
			if (auth) {
				rsm.updateAuthenticator ((BroadcastAuthenticator *) header);
			}
		}
		else
		{
			int CRC = -1;
			if (!auth || rsm.validFrame (header, result, &CRC)) {
				iseq = header->seq;
				k = header->k;	// define k right here
				frame_len = result - CRC;
				decoder = new Decoder (k, FBP_DATA_LEN (frame_len));
				decoder->start ();
				decoder->printProgress (*rx_frames, 1);
				time_begin = getTimestamp ();
				
				break;
			}
		}
	}
	rsm.setIsDecoding();
	int count = 0;

	// Decoding has started, read codewords and Authenticators until completing decoding

	while (!interrupt && !decoder->isComplete ())
	{
		//get frame
		result = if_read (frame_buffer, (uint8_t**) &header, max_frame_size);
		if (result < 0) {
			cout << "Error: reception error, decoding aborted: " << strerror (errno) << endl;
			return -1;
		}

		// increment received frames
		(*rx_frames)++;
		receptionprogress++;

		if (ReceiverSecurityManager::isAuthenticator (header)) {
			// update authenticator
			if (auth) {
				rsm.updateAuthenticator ((BroadcastAuthenticator *) header);
			}
		}
		else if (auth && !rsm.validFrame (header, frame_len, NULL)) {
			if (header->k != k) {
				nOtherInvalid++;
			}
			else  {
				nInvalid++;
				//write ( 2, ".", 1 );
				//fprintf(stderr, "[%d]", header->seq);
			}
		}
		// acceptable frame
		else if (result == frame_len || (adhoc == 0 && result == frame_len + 4)) { // Some kernels give additional 4bytes with CRC
			nValid++;
			//write ( 2, "|", 1 );

			// add frame contente to decoder
			decoder->addCodeword (header->seed, header->degree, ((uint8_t*) header) + FBP_HEADER_LEN);
			lseq = header->seq;

			// update maximum memory usage
			// updateMaxMem();

		}
		else {
			//cout << "Error: received data is not a valid codeword: " << strerror (errno) << endl;
		}

		// print progress
		if (receptionprogress > REFRESH_RATE) {
			decoder->printProgress (nValid + nInvalid, CALCDIFF (iseq, lseq) + 1);
			receptionprogress = 0;
		}
	}

	time_end = getTimestamp ();

	decoder->stop ();

	if (decoder->isComplete () == 0) {
		cout << "Failed to download the file." << endl;
		return -1;
	}

	cout << endl;

	data_size = k * FBP_DATA_LEN (frame_len);
	mappingFileRW (filename, data_size, &data);
	decoder->writeData (data);
	unmappingFile (data_size, data);

	*tx_frames = CALCDIFF (iseq, lseq) + 1;

	if (stats) {
		printf ("File with k=%d decoded with %d codewords in %02f s.\n",
		decoder->getK (), decoder->getTotalCW (),
		(time_end - time_begin));
		printf ("Gooput=%f Mbits (%f MiB/s)\n",
		data_size * 8 / 1000000 / (time_end - time_begin),
		data_size / 1024 / 1024 / (time_end - time_begin));
		if (auth) {
			cout << "Valid codewords: " << dec << nValid << endl;
			cout << "Invalid codewords: " << dec << nInvalid << endl;
		}
		cout << "Lost codewords: " << dec << (CALCDIFF (iseq, lseq) + 1 - nValid - nInvalid) << endl;
		cout << "Total codewords: " << dec << (CALCDIFF (iseq, lseq) + 1) << endl;
		if (auth) {
			cout << "Other Transmission codewords: " << nOtherInvalid << endl;
		}
	}

	fflush (stdout);
	// free and close
	delete decoder;
	close_last_sock ();

	return 0;
}

void printUsage (char *argv0)
{
	cout << "Usage: " << argv0 << " -i <interface> -o <out_file> [-u] [-a]" << endl;
}

int main (int argc, char **argv)
{
	bool auth = true;
	bool adhoc = false;
	int needs = 0x3, c, result;
	uint32_t rx_frames = 0, tx_frames = 0;
	char *ifname = NULL;
	char *filename = NULL;

	cout << "File Broadcast Client" << endl;

	// verify args
	while ((c = getopt (argc, argv, "i:o:auh?")) != -1)
	{
		switch (c)
		{
		case 'i':
			ifname = optarg;
			needs &= 0xfffe;
			break;
		case 'o':
			filename = optarg;
			needs &= 0xfffd;
			break;
		case 'u': // unprotected
			auth = false;
			break;
		case 'a': // use ad hoc instead of promiscuous
			adhoc = true;
			break;
		case '?':
		case 'h':
		default:
			printUsage (argv[0]);
			exit (-1);
		}
	}

	if (needs)
	{
		printUsage (argv[0]);
		exit (-1);
	}

	// set signals handler
	signal (SIGINT, &sighandler);

	// initialize sockets list
	init_sock_list ();

	// start receiving
	if (if_recvfrom (ifname, filename, &rx_frames, &tx_frames, auth, adhoc) < 0) {
		cout << "Failed to download the file." << endl;
		exit (-1);
	}

	// closing socket list
	close_sock_list ();

	cout << "Successfully downloaded the file." << endl;
	cout << "Received " << rx_frames << " codewords out of " << tx_frames << " transmitted." << endl;

	return 0;
}
