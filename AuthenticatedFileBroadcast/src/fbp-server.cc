/*
 * File:   fbp-server.cpp
 * Author: joao
 *
 * Created on April 5, 2013, 8:17 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <sched.h>

#include "file_handler.h"
#include "network_handler.h"
#include "time_handler.h"

#include "Encoder.hh"
#include "RandPerm.hh"
#include "BroadcastSecurityManager.hh"

using namespace std;

bool interrupt = false;

void sighandler (int sig)
{
    if (sig == SIGINT)
    {
        cout << endl << "Interupted by the user.";
        interrupt = true;
    }

    close_sock_list ();
}

int if_sendto (char *ifname, struct ether_addr * dst_addr, char *filename, int frame_len, bool auth, uint32_t r_interval, bool store_key, char *keyfile)
{
    int result, data_len;
    ssize_t tx_bytes = 0;
    unsigned int tx_frames = 0;
    double bitrate;
    unsigned char *frame, *data, *cw_data;
    struct fbp_header *header;
    off_t data_size;
    double time_begin, time_end;
    Encoder *encoder;

    // init socket
    if (if_init_server (ifname, dst_addr) != 0)
    {
        cout << "Error: if_init(" << ifname <<"): " << strerror (errno) << endl;
        return -1;
    }

    // alloc frame memory;
    frame = (unsigned char *) malloc (frame_len + 16);
    cw_data = frame + FBP_HEADER_LEN + 16 - (((unsigned long) frame + FBP_HEADER_LEN) % 16);
    header = (struct fbp_header *) (cw_data - FBP_HEADER_LEN);

    // init encoding-related data
    if (mappingFileRO (filename, &data_size, &data) != 0) {
	return -1;
    }
    encoder = new Encoder (data, data_size, FBP_DATA_LEN (frame_len));
    header->k = encoder->getK ();
    header->seq = 0;
    time_begin = getTimestamp ();

    BroadcastSecurityManager bsm (frame_len, header, encoder, &cw_data, &interrupt);

    if (auth) {
        if (store_key) {
            bsm.storePublickeyHash(keyfile);
	}
        cout << "Reveal the key to whom you want to distribute the file." << endl;
        bsm.displayPublickeyHash();
	bsm.setRInterval(r_interval);
    }
    //cout << "SEND THREAD created" << endl;
    bsm.startGen ();
    bsm.startAuth ();

    printf ("\rrBroadcast bitrate: 0.0 Mb/s    ");
    fflush (stdout);

    while (!interrupt) {
        // send authenticator
        if (auth && bsm.authenticatorAvailable ()) {
            struct BroadcastAuthenticator *authenticator = (struct BroadcastAuthenticator *) bsm.getAuthenticator ();

	    if ((result = if_write ((unsigned char*) authenticator, sizeof (struct BroadcastAuthenticator) )) == -1) {
		cout << "Error: sendto: " << strerror (errno) << endl;
		break;
	    }
	    tx_frames++;
	    tx_bytes += result;
        }

        // send codeword
        if ((result = if_write (bsm.readFrame (), frame_len )) == -1) {
            cout << "Error: transmitte error: " << strerror (errno) << endl;
	    break;
	}
	tx_frames++;
	tx_bytes += result;

        // print bitrate
        if ((tx_frames % REFRESH_RATE) == 0) {
            time_end = getTimestamp ();
            bitrate = ((double) tx_bytes) * 8.0;
            bitrate /= (double) 1000000.0 *(time_end - time_begin);

            printf ("\rBroadcast bitrate: %.2f Mb/s    ", bitrate);
            fflush (stdout);

            tx_bytes = 0;
            time_begin = time_end;
        }
    }

    // free and close
    delete encoder;
    unmappingFile (data_size, data);
    free (frame);
    close_last_sock ();

    return 0;
}

void printUsage (char *argv0)
{
    cout << "Usage: " << argv0 << " -i <interface> -f <file> [-l <frame length>]"
         << " [-d <destination MAC>] [-u] [-r <max random period>] [-s <file>]" << endl;
}

int main (int argc, char **argv)
{
    bool auth = true;
    bool store_key = false;
    uint32_t frame_len = ETH_DATA_LEN;
    uint32_t r_interval = 30;
    char * ifname = NULL;
    char * filename = NULL;
    char * keyfile = NULL;
    struct ether_addr * dst_addr = 0;

    uint32_t needs = 0x3;
    int c;

    cout << "File Broadcast Server" << endl;

    // verify args
    while ((c = getopt (argc, argv, "i:f:d:l:r:s:uh?")) != -1)
    {
        switch (c)
        {
        case 'i':
            ifname = optarg;
            needs &= 0xfffe;
            break;
        case 'f':
            filename = optarg;
            needs &= 0xfffd;
            break;
        case 'd':
            if ((dst_addr = ether_aton (optarg)) == NULL)
            {
                cout << "Invalid argument: destination MAC " << optarg << endl;
                exit (-1);
            }
            break;
        case 'l':
            frame_len = atoi (optarg);
            if (frame_len < FBP_MIN_FRAME_LEN || frame_len > FBP_MAX_FRAME_LEN)
            {
                cout << "Invalid argument: frame length " << optarg << endl;
                exit (-1);
            }
            break;
        case 's':
            store_key = true;
            keyfile = optarg;
            break;
        case 'u': // unprotected
            auth = false;
            break;
        case 'r':
	    if (r_interval < 1) {
		cout << "Invalid argument: max random period " << r_interval << endl;
                return -1;
            } else if (r_interval > N_OF_HASHES) {
                cout << "Invalid argument: max random period " << r_interval << endl;
                return -1;
            }
	    r_interval = atoi(optarg);
	    break;
        case '?':
        case 'h':
        default:
            printUsage (argv[0]);
            exit (-1);
        }
    }

    if (needs)
    {
        printUsage (argv[0]);
        exit (-1);
    }

    // set signals handler
    signal (SIGINT, &sighandler);

    // start sending
    if (if_sendto (ifname, dst_addr, filename, frame_len, auth, r_interval, store_key, keyfile) < 0) {
        cout << endl << "Failed to upload the file." << endl;
        return -1;
    }

    cout << endl << "Successfully uploaded the file." << endl;
    return 0;
}
