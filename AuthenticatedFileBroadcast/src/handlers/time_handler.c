#include <stdlib.h>
#include <sys/time.h>

#include "time_handler.h"

double getTimestamp() {
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return (double) (tv.tv_usec / 1000000.0) + tv.tv_sec;
}
