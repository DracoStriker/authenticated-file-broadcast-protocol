#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <unistd.h>

#ifdef __SSE2__
#include <emmintrin.h>
#endif

#include "lt_handler.h"

void idealSoliton(double *rho, u_int32_t k) {
    u_int32_t i;
    rho[0] = 0.0;
    rho[1] = 1.0 / k;
    for (i = 2; i <= k; i++) rho[i] = 1.0 / (i * (i - 1.0));
    
    //for(i=0; i<=k ; i++) printf("rho[%u] = %f\n", i, rho[i]);
}

void robustSoliton(double *tau, double c, double delta, u_int32_t k) {


    u_int32_t i;
    double S = c * log(k / delta) * sqrt(k);
    //if (S < 0) S++;
    //printf("S = %f\n", round(S));
    //printf("K/S = %f\n", round(k/S));
    tau[0] = 0.0;

    // init tau with 0
    //memset(tau, 0, k * sizeof (double));

    for (i = 1; i < round(k / S) ; i++)
        tau[i] = S / (k * i);
        //tau[i] = ( S / k ) * ( 1.0 / ( i + 1.0 ) );

    tau[i++] = S * log(S / delta) / (double) k;
    //printf("tau[%u++] = %f\n", i-1, tau[i-1]);

    for (; i <= k; i++)
        tau[i] = 0.0;
    
    //for(i=0; i<=k ; i++) printf("tau[%u] = %f\n", i, tau[i]);
}

double calculateZ(double *rho, double *tau, u_int32_t k) {
    double Z = 0.0;
    u_int32_t i;
    for (i = 1; i <= k; ++i)
        Z += rho[i] + tau[i];

    return Z;
}

void normalize(double *u, double *rho, double *tau, u_int32_t k) {
    /*double miu[k];
    u_int32_t i;

    miu[0] = (rho[0] + tau[0]) / Z;
    ro[0] = miu[0];

    for (i = 1; i < k; ++i) {
        miu[i] = (rho[i] + tau[i]) / Z;
        ro[i] = ro[i - 1] + miu[i];
    }*/

    /*u_int32_t i;
    

    ro[0] = (rho[0] + tau[0]) / Z;

    for (i = 1; i < k; ++i)
        ro[i] = ro[i - 1] + ((rho[i] + tau[i]) / Z);*/

    u_int32_t i;
    //Z = 1;
    double Z = calculateZ(rho, tau, k);
    //Z = 0;
    u[0] = 0.0;

    for (i = 1; i <= k; i++) {
        //Z += rho[i] + tau[i];
        u[i] = ((rho[i] + tau[i]) / Z);
    }
}

u_int32_t getRandomDegree(u_int32_t k, double *ro, u_int32_t seed) {

    //if (k > 1000) k = 1000;

    //srandom(seed);
    int32_t rand_val;
    struct random_data rand_databuf = {0};
    char rand_statebuf[PRNG_BUFSZ] = {0};

    initstate_r(seed, rand_statebuf, PRNG_BUFSZ, &rand_databuf);
    random_r(&rand_databuf, &rand_val);

    double r = rand_val / (RAND_MAX + 1.0);


    u_int32_t degree = -1;
    u_int32_t i;
    for (i = 0; i + 1 < k; ++i) {
        if (r >= ro[i] && r < ro[i + 1]) {
            degree = i + 2;
            break;
        } else if (r < ro[i]) {
            degree = i + 1;
            break;
        }
    }

    //free(rand_databuf);
    //free(rand_statebuf);

    return degree > k ? 1 : degree;
}

void XOR(unsigned char* in1, unsigned char* in2, unsigned char* out, unsigned int size) {
    for (unsigned int i = 0; i < size; i++) {
        out[i] = in1[i] ^ in2[i];
    }
}

void XOR_u16(unsigned char* in1, unsigned char* in2, unsigned char* out, unsigned int size) {
    unsigned int i;
    u_int16_t* i1 = (u_int16_t*) in1;
    u_int16_t* i2 = (u_int16_t*) in2;
    u_int16_t* o = (u_int16_t*) out;
    unsigned int nn = size / sizeof (u_int16_t);
    unsigned int dd = sizeof (u_int16_t) * nn;
    for (i = 0; i < nn; i++) o[i] = i1[i] ^ i2[i];
    if (dd < size) XOR(in1 + dd, in2 + dd, out + dd, size - dd);
}

void XOR_u32(unsigned char* in1, unsigned char* in2, unsigned char* out, unsigned int size) {
    unsigned int i;
    u_int32_t* i1 = (u_int32_t*) in1;
    u_int32_t* i2 = (u_int32_t*) in2;
    u_int32_t* o = (u_int32_t*) out;
    unsigned int nn = size / sizeof (u_int32_t);
    unsigned int dd = sizeof (u_int32_t) * nn;
    for (i = 0; i < nn; i++) o[i] = i1[i] ^ i2[i];
    for (i = dd; i < size; i++) out[i] = in1[i] ^ in2[i];

}

void XOR_u64(unsigned char* in1, unsigned char* in2, unsigned char* out, unsigned int size) {
    unsigned int i;
    u_int64_t* i1 = (u_int64_t*) in1;
    u_int64_t* i2 = (u_int64_t*) in2;
    u_int64_t* o = (u_int64_t*) out;
    unsigned int nn = size / sizeof (u_int64_t);
    unsigned int dd = sizeof (u_int64_t) * nn;
    for (i = 0; i < nn; i++) o[i] = i1[i] ^ i2[i];
    for (i = dd; i < size; i++) out[i] = in1[i] ^ in2[i];
}

#ifdef __SSE2__

void XOR_m128(unsigned char* in1, unsigned char* in2, unsigned char* out, unsigned int size) {
    unsigned int i;
    __m128i* i1 = (__m128i*) in1;
    __m128i* i2 = (__m128i*) in2;
    __m128i* o = (__m128i*) out;
    unsigned int nn = size / sizeof (__m128i);
    unsigned int dd = sizeof (__m128i) * nn; 
    for (i = 0; i < nn; i++) o[i] = _mm_xor_si128(i1[i], i2[i]);
    for (i = dd; i < size; i++) out[i] = in1[i] ^ in2[i];
}

#else

void XOR_m128(unsigned char* in1, unsigned char* in2, unsigned char* out, unsigned int size) {
    unsigned int i;
    u_int64_t* i1 = (u_int64_t*) in1;
    u_int64_t* i2 = (u_int64_t*) in2;
    u_int64_t* o = (u_int64_t*) out;
    unsigned int nn = size / sizeof (u_int64_t);
    unsigned int dd = sizeof (u_int64_t) * nn;
    for (i = 0; i < nn; i++) o[i] = i1[i] ^ i2[i];
    for (i = dd; i < size; i++) out[i] = in1[i] ^ in2[i];
}

#endif
