#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <math.h>
#include <fcntl.h>
#include <sys/queue.h>
#include <sys/ioctl.h>
#include <stdbool.h>

#include "network_handler.h"

u_int8_t isListInit = false;
LIST_HEAD(sock_listhead, sock_entry) sl_head;

struct sock_entry {
	int sock;
	LIST_ENTRY(sock_entry) entries;
};

int init_sock_list() {
	LIST_INIT(&sl_head);
	isListInit = true;
	return 0;
}

int close_last_sock() {

	struct sock_entry *node;

	if (!LIST_EMPTY(&sl_head)) {
		node = sl_head.lh_first;
		close(node->sock);
		LIST_REMOVE(node, entries);
		free(node);
	}

	return 0;
}

int close_sock_list() {

	struct sock_entry *node;

	while (!LIST_EMPTY(&sl_head)) {
		node = sl_head.lh_first;
		close(node->sock);
		LIST_REMOVE(node, entries);
		free(node);
	}

	return 0;
}

#define PROMISC_OVERHEAD_MIN 50			// PLPC + MAC + LLC
#define PROMISC_OVERHEAD_MAX 80
#define PACKET_SIZE_MIN 46				// Data 46 - 1500 bytes
static struct sockaddr_ll sock_addr;	// Socket address that will receive codewords in ad hoc mode
static int s;							// Socket to read codewords
static int adhoc = 1;					// Use ad hoc mode, otherwise promicuous
static int promiscuousOverhead = -1;	// Save the Promiscuous overhead value

int if_read(uint8_t * buffer, uint8_t ** offset, uint32_t maxlen)
{
	int result;

	if (adhoc) {
		int addr_len = sizeof(struct sockaddr_ll);

		result = recvfrom(s, buffer, maxlen, 0, (struct sockaddr *) &sock_addr, &addr_len);
		*offset = buffer;
	}
	else {
		unsigned short EtherType = htons(FBP_TYPE);
		
		result = recvfrom (s, buffer, maxlen, 0, 0, 0 );
		if(result == -1) {
			printf("Error: recvfrom: %s\n", strerror(errno));
			return -1;
		}
		if ((promiscuousOverhead == -1 && result > PROMISC_OVERHEAD_MIN + PACKET_SIZE_MIN) || (promiscuousOverhead > -1 && result > promiscuousOverhead + PACKET_SIZE_MIN))
		{
			if(promiscuousOverhead == -1)
			{
				for(int temp = 2; temp < PROMISC_OVERHEAD_MAX && result - temp >= PACKET_SIZE_MIN; temp++)
				{
					if(((uint8_t*)&EtherType)[0] == buffer[temp-2] &&
					((uint8_t*)&EtherType)[1] == buffer[temp-1])
					{
						promiscuousOverhead = temp;
						result -= temp;
						*offset = buffer + temp;
						return result;
					}
				}
			}
			else
			{
				if(((uint8_t*)&EtherType)[0] == buffer[promiscuousOverhead-2] &&
				((uint8_t*)&EtherType)[1] == buffer[promiscuousOverhead-1])
				{
					result -= promiscuousOverhead;
					*offset = buffer + promiscuousOverhead;
					return result;
				}
			}
		}
		*offset = buffer;
		//printf("%d == %d && %d == %d\n", ((uint8_t*)&EtherType)[0], buffer[PROMISC_OVERHEAD-1], ((uint8_t*)&EtherType)[1], buffer[PROMISC_OVERHEAD-2]);
	}

	return result;
}

int if_write ( uint8_t * buffer, uint32_t len )
{
	return sendto ( s, buffer, len, 0, (struct sockaddr *) &sock_addr, sizeof (sock_addr) );
}

int if_init_server ( char * ifname, struct ether_addr * dst_addr )
{
	// Create socket
	s = socket(AF_PACKET, SOCK_DGRAM, htons(FBP_TYPE));

	if (s == -1) {
		printf("Error: socket: %s\n", strerror(errno));
		return -1;
	}
	
	// Set socket-related parameters

	struct sockaddr_ll addr = {0};
	struct ifreq ifr;
	int ifindex;

	if ((ifindex = if_nametoindex(ifname)) <= 0) {
		printf("Error: if_nametoindex: %s\n", strerror(errno));
		close(s);
		return -3;
	}

	// set socket address
	sock_addr.sll_family = AF_PACKET;
	sock_addr.sll_protocol = htons(FBP_TYPE);
	sock_addr.sll_ifindex = ifindex;
	sock_addr.sll_halen = ETHER_ADDR_LEN;
	if (dst_addr) { // Use specific destination address
		memcpy(sock_addr.sll_addr, dst_addr->ether_addr_octet, ETHER_ADDR_LEN);
	}
	else { // Otherwise use broadcast address
		sock_addr.sll_pkttype = PACKET_BROADCAST;
		memset(sock_addr.sll_addr, 0xFF, ETHER_ADDR_LEN);
	}

	// Set socket maximum MTU
	ifr.ifr_mtu = FBP_MAX_FRAME_LEN;
	memset ( ifr.ifr_name, 0, sizeof(ifr.ifr_name) );
	memcpy(ifr.ifr_name, ifname, strlen(ifname)); 

	if (ioctl(s, SIOCSIFMTU, (caddr_t)&ifr) < 0) {
		printf("Error: ioctl(SIOCSIFMTU): %s\n", strerror(errno));
		close(s);
		return -4;
	}

	if (ioctl(s, SIOCGIFTXQLEN, (caddr_t)&ifr) < 0) {
		printf("Error: ioctl(SIOCGIFTXQLEN): %s\n", strerror(errno));
	}
	else {
		printf("TX queue length = %d\n", ifr.ifr_qlen );
	}

	ifr.ifr_qlen = 50000; /* Set queue to a very long value */

	if (ioctl(s, SIOCSIFTXQLEN, (caddr_t)&ifr) < 0) {
		printf("Error: ioctl(SIOCSIFTXQLEN): %s\n", strerror(errno));
	}
	else {
		printf("TX queue length updated\n" );
	}

	if (ioctl(s, SIOCGIFTXQLEN, (caddr_t)&ifr) < 0) {
		printf("Error: ioctl(SIOCGIFTXQLEN): %s\n", strerror(errno));
	}
	else {
		printf("TX queue length = %d\n", ifr.ifr_qlen );
	}

	return 0;
}

int if_init_client(char * ifname, uint32_t * maxlen, int promiscuous)
{
	// Create socket
	if (promiscuous) {
		adhoc = 0;
		s = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
		*maxlen = FBP_MAX_FRAME_LEN + PROMISC_OVERHEAD_MAX;
	}
	else {
		s = socket(AF_PACKET, SOCK_DGRAM, htons(FBP_TYPE));
		*maxlen = FBP_MAX_FRAME_LEN;
	}

	if (s == -1) {
		printf("Error: socket: %s\n", strerror(errno));
		return -1;
	}
	
	// Set socket-interface
	
	// This version only works for AF_INET and not AF_PACKET (Removed)
	/*if (setsockopt ( s , SOL_SOCKET , SO_BINDTODEVICE , ifname , strlen(ifname) + 1 ) == -1) {
		printf("Error: setsockopt(SO_BINDTODEVICE): %s\n", strerror(errno));
		return -2;
	}*/
	// This version works for AF_PACKET
	struct sockaddr_ll s_addr;
	memset(&s_addr, 0, sizeof(s_addr));
	s_addr.sll_family = AF_PACKET;
	if (promiscuous) {
		s_addr.sll_protocol = htons(ETH_P_ALL);
	}
	else {
		s_addr.sll_protocol = htons(FBP_TYPE);
	}
	s_addr.sll_ifindex = if_nametoindex(ifname);
	if ( bind ( s , (struct sockaddr*) &s_addr,  sizeof(s_addr) ) == -1 ) {
		printf("Error: bind( %s ): %s\n", ifname, strerror(errno));
		return -2;
	}

	struct ifreq ethreq;

	memset ( ethreq.ifr_name, 0, sizeof(ethreq.ifr_name) );
	strncpy ( ethreq.ifr_name, ifname, strlen(ifname) + 1 );
	//strcpy ( ethreq.ifr_name, ifname );
	if(ioctl ( s, SIOCGIFFLAGS, &ethreq ) == -1) {
		printf("Error: ioctl(SIOCGIFFLAGS): %s\n", strerror(errno));
		return -3;
	}
	ethreq.ifr_flags |= IFF_PROMISC;
	if(ioctl ( s, SIOCGIFFLAGS, &ethreq ) == -1) {
		printf("Error: ioctl(SIOCGIFFLAGS): %s\n", strerror(errno));
		return -4;
	}

	if (isListInit) {
		struct sock_entry * new_sock = (struct sock_entry*) malloc(sizeof (struct sock_entry));
		new_sock->sock = s;
		LIST_INSERT_HEAD(&sl_head, new_sock, entries);
	}
	
	// Set a lot of receiving buffers
	long bufsize=50000;
	if (setsockopt ( s, SOL_SOCKET, SO_RCVBUFFORCE, &bufsize, sizeof(bufsize) ) == -1) {
		printf("Error: setsockopt(SO_RCVBUFFORCE): %s\n", strerror(errno));
	}
	
	return 0;
}

void empty_sock() {

	int len;
	unsigned int len_size = sizeof(len);
	int flags;

	getsockopt(s, SOL_SOCKET, SO_RCVBUF, (void *)&len, &len_size);

	char *garbage = (char *) malloc(len);

	// Set socket to nonblocking to avoid staing blocked after last packet
	flags = fcntl(s, F_GETFL, 0);
	if (fcntl(s, F_SETFL, flags|O_NONBLOCK) == -1) {
		printf("Error: fcntl(F_SETFL): %s\n", strerror(errno));
	}

	while (read ( s, garbage, len ) > 0) { }

	// Set socket to block to normal operation
	if (fcntl(s, F_SETFL, flags & ~O_NONBLOCK) == -1) {
		printf("Error: fcntl(F_SETFL): %s\n", strerror(errno));
	}

	free ( garbage );
}
