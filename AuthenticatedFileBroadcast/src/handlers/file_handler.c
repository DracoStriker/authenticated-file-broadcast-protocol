#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>

#include "file_handler.h"

int mappingFileRW(char *fileName, off_t size, unsigned char **data) {

    int fd;
    void* ptr;

    // open file
    fd = open(fileName, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU | S_IRWXG | S_IRWXO);
    if (fd == -1) {
        printf("Error: mappingFileRW: open: %s\n", strerror(errno));
        return -1;
    }
    
    // set file size
    if (lseek(fd, size - 1, SEEK_SET) == -1 || write(fd, "", 1) != 1) {
        printf("Error: mappingFileRW: lseek: %s\n", strerror(errno));
        close(fd);
        return -2;
    }
    
    // map file
    ptr = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (ptr == MAP_FAILED) {
        printf("Error: mappingFileRW: mmap: %s\n", strerror(errno));
        close(fd);
        return -3;
    }
    
    // close file
    if (close(fd) == -1) {
        printf("Error: mappingFileRW: close: %s\n", strerror(errno));
        return -4;
    }
    
    *data = (unsigned char*) ptr;
    return 0;
}

int mappingFileRO(char *fileName, off_t *size, unsigned char **data) {

    int fd;
    off_t fileSize;
    void* ptr;

    // open file
    if ((fd = open(fileName, O_RDONLY)) == -1) {
        printf("Error: mappingFileRO: open: %s\n", strerror(errno));
        return -1;
    }

    // get file size
    if ((fileSize = lseek(fd, 0, SEEK_END)) == -1) {
        printf("Error: mappingFileRO: lseek: %s\n", strerror(errno));
        close(fd);
        return -2;
    }

    if (fileSize < 1) {
		printf("Error: file cannot be empty\n");
        close(fd);
        return -3;
	}

    // map file
    ptr = mmap(NULL, fileSize, PROT_READ, MAP_PRIVATE | MAP_POPULATE , fd, 0);
    //ptr = mmap (NULL, fileSize, PROT_READ, MAP_SHARED|MAP_32BIT, fd, 0);
    if (ptr == MAP_FAILED) {
        printf("Error: mappingFileRO: mmap: %s\n", strerror(errno));
        close(fd);
        return -4;
    }

    // close file
    if (close(fd) == -1) {
        printf("Error: mappingFileRO: close: %s\n", strerror(errno));
        return -5;
    }

    *data = (unsigned char*) ptr;
    *size = fileSize;
    return 0;
}

int unmappingFile(off_t size, unsigned char * data) {

    if (size > 0 && munmap(data, size) == -1) {
        printf("Error: unmappingFile: munmap: %s\n", strerror(errno));
        return -1;
    }

    return 0;
}

int memFileRO(char *fileName, off_t *size, unsigned char **data) {

    int fd;
    off_t fileSize;
    void* ptr;

    // open file
    fd = open(fileName, O_RDONLY);
    if (fd == -1) {
        printf("Error: memFileRO: open: %s\n", strerror(errno));
        return -1;
    }

    // get file size
    if ((fileSize = lseek(fd, 0, SEEK_END)) == -1) {
        printf("Error: memFileRO: lseek: %s\n", strerror(errno));
        close(fd);
        return -2;
    }

    lseek(fd, 0, SEEK_SET);

    // read file
    ptr = (unsigned char*) malloc(fileSize);
    if (read(fd, ptr, fileSize) == -1) {
        printf("Error: memFileRO: read: %s\n", strerror(errno));
        close(fd);
        return -3;
    }

    // close file
    if (close(fd) == -1) {
        printf("Error: memFileRO: close: %s\n", strerror(errno));
        return -4;
    }

    *data = (unsigned char*) ptr;
    *size = fileSize;
    return 0;
}
