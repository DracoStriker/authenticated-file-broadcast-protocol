/* 
 * File:   file_handler.h
 * Author: joao
 *
 * Created on April 5, 2013, 11:47 PM
 */

#ifndef FILE_HANDLER_H
#define	FILE_HANDLER_H

#ifdef	__cplusplus
extern "C" {
#endif

// map file for read and write
int mappingFileRW(char *fileName, off_t size, unsigned char **data );
// map file for read only 
int mappingFileRO(char *fileName, off_t *size, unsigned char **data);
// unmap file
int unmappingFile(off_t size, unsigned char * data);

// read file to memory
int memFileRO(char *fileName, off_t *size, unsigned char **data);
int fmemFileRO(char *fileName, off_t *size, unsigned char **data);

#ifdef	__cplusplus
}
#endif

#endif	/* FILE_HANDLER_H */

