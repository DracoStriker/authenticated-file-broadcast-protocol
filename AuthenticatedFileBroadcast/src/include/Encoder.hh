/* 
 * File:   Encoder.h
 * Author: joao
 *
 * Created on April 6, 2013, 6:41 PM
 */

#ifndef ENCODER_H
#define	ENCODER_H

#define MAX_CW_QUEUE_SIZE 1000
#include <queue>
#include <vector>
#include <random>
#include <pthread.h>
#include "Codeword.hh"
#include "RandPerm.hh"

class Encoder {
    unsigned char* data;
    off_t data_size, cw_size;
    u_int32_t seed;
    double c, delta;
    u_int32_t k;
    double *rho, *tau;
    vector<double>* p;
    RandPerm* rp;
    default_random_engine generator;
    discrete_distribution<u_int32_t>* distribution;
public:
    Encoder(unsigned char* data, off_t data_size, off_t chunk_size);
    Encoder(unsigned char* data, off_t data_size, off_t chunk_size, double c, double delta);
    virtual ~Encoder();
    int genCodeword(u_int32_t* cw_seed, u_int32_t* cw_degree, unsigned char** cw_data);
    u_int32_t getK();
private:
    off_t getCodewordSize(u_int32_t index);

};

#endif	/* ENCODER_H */

