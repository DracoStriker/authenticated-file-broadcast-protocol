/* 
 * File:   Decoder.h
 * Author: joao
 *
 * Created on April 8, 2013, 11:49 PM
 */

#ifndef DECODER_H
#define	DECODER_H

#include <pthread.h>
#include <queue>
#include <stack>

#include "Codeword.hh"
#include "Symbol.hh"
#include "RandPerm.hh"
#include "Logger.hh"

using namespace std;

class Decoder {
    bool end;
    unsigned char* data;
    u_int32_t k;
    off_t cw_size;
    u_int32_t cw_decoded;
    u_int32_t cw_total;
    u_int32_t cw_mem;
    queue<Codeword*> recv_queue;
    Symbol* symbols;
    pthread_mutex_t decodeMutex;
    pthread_cond_t decodeCondition;
    pthread_t decodeThread;
    RandPerm* rp;

public:
    Decoder(unsigned char* data, u_int32_t k, off_t cw_size);
    Decoder(u_int32_t k, off_t cw_size);
    virtual ~Decoder();
    void start(void);
    void stop(void);
    bool isComplete(void);
    u_int32_t getK();
    u_int32_t getTotalCW();
    u_int32_t getDecodedCW();
    int addCodeword(u_int32_t cw_seed, u_int32_t cw_degree, unsigned char* cw_data);
    int insertCodeword(Codeword* c);
    void writeData();
    void writeData(unsigned char* fdata);
    void printData();
    void printProgress(unsigned int total, unsigned int loss);
private:
    static void* threadHandler(void* pthis);
    void decodeCycle(void);
};

#endif	/* DECODER_H */

