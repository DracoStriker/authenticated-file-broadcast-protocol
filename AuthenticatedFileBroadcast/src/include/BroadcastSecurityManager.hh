/* 
 * File:   BroadcastSecurityManager.hpp
 * Author: simon
 *
 * Created on October 24, 2014, 2:28 AM
 */

#ifndef BROADCASTSECURITYMANAGER_HPP
#define	BROADCASTSECURITYMANAGER_HPP

#include <openssl/sha.h>
#include <openssl/rsa.h>
#include <pthread.h>
#include <semaphore.h>

#include <iostream>

#include "Encoder.hh"
#include "ether_fbp.h"

#define RSA_KLEN  1024
#define PUB_EXP      3
#define N_OF_HASHES 61 //(ETH_DATA_LEN - 2 * RSA_KLEN / 8 - sizeof(uint32_t) * 2) / SHA_DIGEST_LENGTH
#define N_PHASES     4
#define N_THREADS    2

using namespace std;

struct gthr_args {
    struct fbp_header* header;
    Encoder* encoder;
    unsigned char **cw_data;
    int frame_len;
    void *bsm;
};

struct athr_args {
    int frame_len;
    void *bsm;
};

struct BroadcastAuthenticator {
    uint32_t lseq;
    uint32_t code;
    uint8_t hash[N_OF_HASHES][SHA_DIGEST_LENGTH];
    uint8_t sign[RSA_KLEN / 8];
    union {
        uint8_t pkey[RSA_KLEN / 8];
        uint32_t sid;
    };
};

class BroadcastSecurityManager {
public:
    BroadcastSecurityManager(uint32_t frame_len, struct fbp_header* header, Encoder* encoder, unsigned char **cw_data, bool *interrupt);
    virtual ~BroadcastSecurityManager();
    void writeFrame(uint8_t *packet, uint32_t frame_len);
    uint8_t *readFrame();
    void signFrames(uint32_t frame_len);
    uint8_t *getAuthenticator();
    void displayPublickeyHash();
    void storePublickeyHash(char *keyfile);
    bool authenticatorAvailable();
    void setRInterval(uint32_t r_interval);
    void startGen();
    void startAuth();
private:
    int randRange();
    RSA *keypair;
    struct BroadcastAuthenticator authenticator[N_OF_HASHES * N_PHASES];
    bool auth[N_OF_HASHES * N_PHASES];
    uint8_t *frame[N_OF_HASHES * N_PHASES];
    uint32_t rIdx;
    uint32_t wIdx;
    uint32_t aIdx;
    uint32_t maxIdx;
    uint32_t prev_maxIdx;
    uint32_t lseq;
    uint32_t r_interval;
    const int bufferSize;
    size_t dataLen;
    size_t rsaLen;
    sem_t asem;
    sem_t gsem;
    sem_t ssem;
    pthread_t gthread;
    pthread_t athread;
    gthr_args g_args;
    athr_args a_args;
    uint32_t n_of_hashes;
    uint32_t D;
};

#endif	/* BROADCASTSECURITYMANAGER_HPP */
