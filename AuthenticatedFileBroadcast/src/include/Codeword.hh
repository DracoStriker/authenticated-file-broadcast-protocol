/* 
 * File:   Codeword.h
 * Author: joao
 *
 * Created on April 9, 2013, 3:48 PM
 */

#ifndef CODEWORD_H
#define	CODEWORD_H

#include <list>
#include <unistd.h>

using namespace std;

class Codeword {
    //static u_int32_t count;
    unsigned char* data;
    off_t data_size;
    u_int32_t degree;
    u_int32_t index;
    u_int32_t seed;
    
public:
    Codeword(u_int32_t cw_seed, u_int32_t cw_degree, u_int32_t cw_index, unsigned char* cw_data, off_t cw_data_size);
    Codeword(u_int32_t cw_seed, u_int32_t cw_degree, unsigned char* cw_data, off_t cw_data_size);
    virtual ~Codeword();
    void addDepIndex(u_int32_t index);
    int removeDependency(Codeword* sym);
    u_int32_t decrementDegree();
    bool isSymbol();
    u_int32_t getDegree();
    u_int32_t getIndex();
    u_int32_t getSeed();
    unsigned char* getData();
    void printCodeword();
private:
};

//u_int32_t Codeword::count = 0;

#endif	/* CODEWORD_H */

