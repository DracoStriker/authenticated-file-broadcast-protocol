/* 
 * File:   ether_fbp.h
 * Author: joao
 *
 * Created on April 5, 2013, 7:50 PM
 */

#ifndef ETHER_FBP_H
#define	ETHER_FBP_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <net/ethernet.h>

#define FBP_MAX_FRAME_LEN       2304
#define FBP_MIN_FRAME_LEN       42
#define FBP_TYPE                0x1986
#define FBP_HEADER_LEN          ( (int) sizeof(struct fbp_header) )
#define FBP_DATA_LEN(fl)        ( (int) ((fl) - (FBP_HEADER_LEN)) )


#pragma pack(1) 
struct fbp_header {
    u_int32_t seq;
    u_int32_t k;
    u_int32_t seed;
    u_int32_t degree;
};
#pragma pack(0) 

#ifdef	__cplusplus
}
#endif

#endif	/* ETHER_FBP_H */

