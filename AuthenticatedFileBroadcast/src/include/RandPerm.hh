        /* 
 * File:   RandPerm.h
 * Author: joao
 *
 * Created on April 10, 2013, 2:02 AM
 */

#ifndef RANDPERM_H
#define	RANDPERM_H

#include <stdlib.h>

#define PRNG_BUFSZ 32

class RandPerm {
    u_int32_t n; // total number of "cards" in deck
    u_int32_t r; // number of remaining cards
    u_int32_t *deck; // the deck
    struct random_data* rand_databuf;
    char* rand_statebuf;
public:
    RandPerm(u_int32_t num);
    virtual ~RandPerm();
    void setSeed(u_int32_t seed);
    u_int32_t getPerm();
    void permRestore();
private:

};

#endif	/* RANDPERM_H */

