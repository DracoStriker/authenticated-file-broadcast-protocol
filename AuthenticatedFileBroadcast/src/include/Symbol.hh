/* 
 * File:   Symbol.h
 * Author: joao
 *
 * Created on April 9, 2013, 6:35 PM
 */

#ifndef SYMBOL_H
#define	SYMBOL_H

#include <list>
#include <stack>
#include <queue>
#include "Codeword.hh"


using namespace std;

class Symbol {
    Codeword* decoded_word;
    list<Codeword*> codewords;
public:
    Symbol();
    virtual ~Symbol();
    bool isDecoded();
    void addCodeword(Codeword* cw);
    unsigned char* getData();
    Codeword* getDecodeword();
    bool setDecodeword(Codeword* cw, queue<Codeword*>* new_symbols);
    void printSymbol();
private:

};

#endif	/* SYMBOL_H */

