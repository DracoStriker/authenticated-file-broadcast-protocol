/* 
 * File:   time_handler.h
 * Author: joao
 *
 * Created on April 14, 2013, 11:12 PM
 */

#ifndef TIME_HANDLER_H
#define	TIME_HANDLER_H

#ifdef	__cplusplus
extern "C" {
#endif

double getTimestamp();

#ifdef	__cplusplus
}
#endif

#endif	/* TIME_HANDLER_H */

