/* * File:   ReceiverSecurityManager.hpp
 * Author: simon
 *
 * Created on October 25, 2014, 3:02 PM
 */

#ifndef RECEIVERSECURITYMANAGER_HPP
#define	RECEIVERSECURITYMANAGER_HPP

#include "BroadcastSecurityManager.hh"
#include "ether_fbp.h"

#define N_TO_FORGET 128

class ReceiverSecurityManager {
public:
    ReceiverSecurityManager();
    virtual ~ReceiverSecurityManager();
    bool hasPublicKey();
    void setPublickey(BroadcastAuthenticator* authenticator);
    void forgetPublicKey(BroadcastAuthenticator* authenticator);
    bool validPublicKey(BroadcastAuthenticator* authenticator);
    static bool isAuthenticator(struct fbp_header* header);
    void updateAuthenticator(BroadcastAuthenticator* authenticator);
    bool validFrame(struct fbp_header* header, uint32_t frame_len, int* CRC);
    static void displayPublickeyHash(BroadcastAuthenticator* authenticator);
    void setIsDecoding();
private:
    bool replayAttack(uint32_t seq);
    RSA *keypair;
    uint8_t forget[N_TO_FORGET][SHA_DIGEST_LENGTH];
    uint32_t nForget;
    uint32_t seq;
    uint32_t lseq;
    uint32_t sid;
    bool pkset;
    bool decoding;
    const int bufferSize;
    size_t dataLen;
    size_t hashLen;
    size_t rsaLen;
    uint8_t hash[N_OF_HASHES][SHA_DIGEST_LENGTH];
};

#endif	/* RECEIVERSECURITYMANAGER_HPP */
