/* 
 * File:   network_handler.h
 * Author: joao
 *
 * Created on April 5, 2013, 9:08 PM
 */

#ifndef NETWORK_HANDLER_H
#define	NETWORK_HANDLER_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <net/if.h>
#include <netinet/in.h>
#include <netinet/ether.h>
#include <netpacket/packet.h>

#include "ether_fbp.h"

#define REFRESH_RATE            1000
#define WAIT_RESULTS_TIME       20.0
#define USER_MAX_RETRIES        15
#define ACK_TIMEOUT             0.5
#define RECEIVE_TIMEOUT         5.0
#define NO_TIMEOUT              0.0
#define CALCDIFF(x,y)           ((x) < (y) ? (y) - (x) : 0xFFFFFFFF - (x) + (y))

int init_sock_list();
int close_last_sock();
int close_sock_list();
int if_init_client(char * ifname, uint32_t * maxlen, int promiscuous);
int if_init_server(char * ifname, struct ether_addr * dst);
int if_read(uint8_t * buffer, uint8_t ** offset, uint32_t maxlen);
int if_write(uint8_t * buffer, uint32_t len);
void empty_sock();

#ifdef	__cplusplus
}
#endif

#endif	/* NETWORK_HANDLER_H */

