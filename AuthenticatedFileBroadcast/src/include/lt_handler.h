/* 
 * File:   lt_handler.h
 * Author: joao
 *
 * Created on April 9, 2013, 12:26 AM
 */

#ifndef LT_HANDLER_H
#define	LT_HANDLER_H

#ifdef	__cplusplus
extern "C" {
#endif

    #define _USE_MATH_DEFINES
    #define PRNG_BUFSZ 32

    void idealSoliton(double *rho, u_int32_t k);
    void robustSoliton(double *tau, double c, double delta, u_int32_t k);
    double calculateZ(double *rho, double *tau, u_int32_t k);
    void normalize(double *ro, double *rho, double *tau, u_int32_t k);
    u_int32_t getRandomDegree(u_int32_t k, double *ro, u_int32_t seed);

    void XOR(unsigned char* in1, unsigned char* in2, unsigned char* out, unsigned int size);
    void XOR_u16(unsigned char* in1, unsigned char* in2, unsigned char* out, unsigned int size);
    void XOR_u32(unsigned char* in1, unsigned char* in2, unsigned char* out, unsigned int size);
    void XOR_u64(unsigned char* in1, unsigned char* in2, unsigned char* out, unsigned int size);
    void XOR_m128(unsigned char* in1, unsigned char* in2, unsigned char* out, unsigned int size);


#ifdef	__cplusplus
}
#endif

#endif	/* LT_HANDLER_H */

