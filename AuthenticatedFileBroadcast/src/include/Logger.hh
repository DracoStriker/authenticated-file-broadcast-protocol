/* 
 * File:   Logger.h
 * Author: joao
 *
 * Created on April 19, 2013, 12:39 AM
 */

#ifndef LOGGER_H
#define	LOGGER_H

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

class Logger {
    FILE* fp;
public:
    Logger(const char* filename, const char* modes);
    virtual ~Logger();
    void append(const char* msg, ...);
private:

};

#endif	/* LOGGER_H */

