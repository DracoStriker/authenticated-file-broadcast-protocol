/* 
 * File:   ReceiverSecurityManager.cpp
 * Author: simon
 * 
 * Created on October 25, 2014, 3:02 PM
 */

#include <openssl/obj_mac.h>
#include <openssl/err.h>
#include <cstring>

#include "ReceiverSecurityManager.hh"

ReceiverSecurityManager::ReceiverSecurityManager() : bufferSize(N_OF_HASHES) {
	keypair = RSA_new();
	uint8_t exp = PUB_EXP;
#if OPENSSL_VERSION_NUMBER < 0x10100000L
	keypair->e = BN_bin2bn(&exp, 1, NULL);
#else
	RSA_set0_key(keypair, NULL, BN_bin2bn(&exp, 1, NULL), NULL);
#endif
	seq = 0;
	pkset = false;
	lseq = 0;
	rsaLen = RSA_KLEN / 8;
	dataLen = sizeof (struct BroadcastAuthenticator) -rsaLen * 2;
	hashLen = dataLen - 2 * sizeof(uint32_t);
	nForget = 0;
	decoding = false;
}

ReceiverSecurityManager::~ReceiverSecurityManager() {
    RSA_free(keypair);
}

bool ReceiverSecurityManager::hasPublicKey() {
    return pkset;
}

void ReceiverSecurityManager::setPublickey(BroadcastAuthenticator* authenticator) {
#if OPENSSL_VERSION_NUMBER < 0x10100000L
	keypair->n = BN_bin2bn(authenticator->pkey, rsaLen, NULL);
#else
	RSA_set0_key(keypair, BN_bin2bn(authenticator->pkey, rsaLen, NULL), NULL, NULL);
#endif
	pkset = true;
	sid = authenticator->sid;
}

void ReceiverSecurityManager::forgetPublicKey(BroadcastAuthenticator* authenticator) {
    SHA1(authenticator->pkey, RSA_KLEN / 8, forget[nForget]);
    nForget++;
}

bool ReceiverSecurityManager::validPublicKey(BroadcastAuthenticator* authenticator) {
	uint8_t digest [SHA_DIGEST_LENGTH];
	SHA1(authenticator->pkey, RSA_KLEN / 8, digest);
	for (int i = 0; i < nForget; i++)
		if (memcmp(digest, forget[i], SHA_DIGEST_LENGTH) == 0) return false;
	return true;
}

bool ReceiverSecurityManager::isAuthenticator(struct fbp_header* header) {
	return header->k == 0;
}

void ReceiverSecurityManager::updateAuthenticator(BroadcastAuthenticator* authenticator) {
	if ((authenticator->lseq > lseq) && (authenticator->sid == sid)) {
		uint8_t digest[SHA256_DIGEST_LENGTH];
		SHA256((uint8_t *) authenticator, dataLen, digest);
		if (RSA_verify(NID_sha256, digest, sizeof (digest), authenticator->sign, rsaLen, keypair)) {
			memcpy(hash, authenticator->hash, hashLen); // sizeof(authenticator->hash)
			lseq = authenticator->lseq;
			//write ( 2, "A", 1 );
		}
	}
}

bool ReceiverSecurityManager::validFrame(struct fbp_header* header, uint32_t frame_len, int *CRC) {
	uint32_t idx = header->seq - lseq + bufferSize -1;

	if (idx < 0 || idx >= bufferSize) {
		//write ( 2, ">", 1 );
		return false;
	}
	if (decoding && replayAttack(header->seq)) {
		//write ( 2, "r", 1 );
		return false;
	}
	uint8_t digest[SHA_DIGEST_LENGTH];
	if(CRC == NULL)
	{
		SHA1((uint8_t *) header, frame_len, digest);
		
		if(memcmp(digest, hash[idx], SHA_DIGEST_LENGTH) == 0)
		{
			//write ( 2, "N", 1 );
			return true;
		}
	}
	else if(*CRC == -1)
	{
		uint8_t digest[SHA_DIGEST_LENGTH];
		for(*CRC = 0; *CRC <= 32; *CRC += 4) // CRC should be always 4 (0 if there are no CRC), just for future precaution
		{
			SHA1((uint8_t *) header, frame_len - *CRC, digest);
			if(memcmp(digest, hash[idx], SHA_DIGEST_LENGTH) == 0)
			{
				//write ( 2, "N", 1 );
				return true;
			}
		}
		*CRC = -1;
		return false;
	}
	else
	{
		SHA1((uint8_t *) header, frame_len - *CRC, digest);
		if(memcmp(digest, hash[idx], SHA_DIGEST_LENGTH) == 0)
		{
			//write ( 2, "C", 1 );
			return true;
		}
		else
		{
			//write ( 2, "I", 1 );
			return false;
		}
	}
}

bool ReceiverSecurityManager::replayAttack(uint32_t seq) {
	if (seq > this->seq) {
		this->seq = seq;
		return false;
	}
	return true;
}

void ReceiverSecurityManager::displayPublickeyHash(BroadcastAuthenticator* authenticator) {
	uint8_t digest [SHA_DIGEST_LENGTH];
	SHA1(authenticator->pkey, RSA_KLEN / 8, digest);
	cout << "Public Key SHA1: ";
	for (int i = 0; i < SHA_DIGEST_LENGTH; i++) cout << uppercase << hex << (int) digest[i];
	cout << endl;
}

void ReceiverSecurityManager::setIsDecoding() {
	this->decoding = true;
}
