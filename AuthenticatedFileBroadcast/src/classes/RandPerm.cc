/* 
 * File:   RandPerm.cpp
 * Author: joao
 * 
 * Created on April 10, 2013, 2:02 AM
 */

#include "RandPerm.hh"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

RandPerm::RandPerm(u_int32_t num) {
    this->n = num;
    this->deck = new u_int32_t [num];
    permRestore();

    this->rand_databuf = (struct random_data*) malloc(sizeof (struct random_data));
    memset(rand_databuf, 0, sizeof (struct random_data));
    this->rand_statebuf = (char*) malloc(PRNG_BUFSZ);
    memset(rand_statebuf, 0, PRNG_BUFSZ);
    initstate_r(random(), rand_statebuf, PRNG_BUFSZ, rand_databuf);
}

RandPerm::~RandPerm() {
    delete[] deck;
    free(rand_databuf);
    free(rand_statebuf);
}

void RandPerm::setSeed(u_int32_t seed) {
    srandom_r(seed, rand_databuf);
    permRestore();
}

u_int32_t RandPerm::getPerm() {
    u_int32_t result;
    u_int32_t i;
    int32_t rand_val;

    random_r(rand_databuf, &rand_val);
    i = rand_val % r;

    result = deck[i] == 0xFFFFFFFF ? i : deck[i];
    deck[i] = deck[--r] == 0xFFFFFFFF ? r : deck[r];

    return result;
}

void RandPerm::permRestore() {
    r = n;

    memset(deck, 0xFFFFFFFF, n * sizeof (u_int32_t));
}
