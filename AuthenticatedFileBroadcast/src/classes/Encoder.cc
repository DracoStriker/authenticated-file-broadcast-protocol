/* 
 * File:   Encoder.cpp
 * Author: joao
 * 
 * Created on April 6, 2013, 6:41 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>

#include "Encoder.hh"

#include "lt_handler.h"

Encoder::Encoder(unsigned char* data, off_t data_size, off_t cw_size) {
    this->data = data;
    this->data_size = data_size;
    this->cw_size = cw_size;

    this->k = (u_int32_t) data_size / cw_size + (data_size % cw_size > 0);
    this->c = 0.03;
    this->delta = 0.5;
    this->rho = new double[k + 1];
    this->tau = new double[k + 1];
    this->p = new vector<double>(k + 1, 0);

    // LT Distribution
    idealSoliton(rho, k);
    robustSoliton(tau, c, delta, k);
    normalize(p->data(), rho, tau, k);

    this->distribution = new discrete_distribution<u_int32_t>(p->begin()++, p->end());

    this->rp = new RandPerm(k);

    int fd = open("/dev/urandom", O_RDONLY);
    int res = read(fd, &seed, sizeof (seed));

    if (res != sizeof(seed)) exit( 1 );

    this->rp->setSeed(seed);
}

Encoder::Encoder(unsigned char* data, off_t data_size, off_t cw_size, double c, double delta) {
    this->data = data;
    this->data_size = data_size;
    this->cw_size = cw_size;

    this->k = (u_int32_t) data_size / cw_size + (data_size % cw_size > 0);
    this->c = c;
    this->delta = delta;
    this->rho = new double[k + 1];
    this->tau = new double[k + 1];
    this->p = new vector<double>(k + 1, 0);

    // LT Distribution
    idealSoliton(rho, k);
    robustSoliton(tau, c, delta, k);
    normalize(p->data(), rho, tau, k);

    this->distribution = new discrete_distribution<u_int32_t>(p->begin()++, p->end());

    this->rp = new RandPerm(k);

    int fd = open("/dev/urandom", O_RDONLY);
    int res = read(fd, &seed, sizeof ( u_int32_t));
    
    if (res != sizeof(seed)) exit( 1 );

    this->rp->setSeed(seed);
}

Encoder::~Encoder() {
    delete[] this->rho;
    delete[] this->tau;
    delete rp;
    delete distribution;
    delete p;
}

off_t Encoder::getCodewordSize(u_int32_t index) {
    return index == k - 1 && data_size % cw_size ? data_size % cw_size : cw_size;
}

int Encoder::genCodeword(u_int32_t* cw_seed, u_int32_t* cw_degree, unsigned char** cw_data) {
    u_int32_t i;
    u_int32_t index;
    u_int32_t degree;
    unsigned char* ptr_data;
    unsigned char* chunk_data = *cw_data;

    /*int fd = open("/dev/urandom", O_RDONLY);
    read(fd, &seed, sizeof ( u_int32_t));
     rp->setSeed(seed);*/

    // increment seed
    *cw_seed = seed;

    // set seed and degree;
    //*cw_degree = degree = getRandomDegree(k, p->data(), seed);
    *cw_degree = degree = (*distribution)(generator);

    // clear chunk_data
    memset(chunk_data, 0, cw_size);

    // get random index and coding, repeat degree times
    rp->setSeed(seed);
    for (i = 0; i < degree; i++) {
        index = rp->getPerm();
        ptr_data = data + (index * cw_size);

        // coding with XOR
        XOR_u64(chunk_data, ptr_data, chunk_data, getCodewordSize(index));
    }

    seed += degree;

    return 0;
}

u_int32_t Encoder::getK() {
    return k;
}

