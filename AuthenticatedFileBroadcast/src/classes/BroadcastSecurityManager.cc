/* 
 * File:   BroadcastSecurityManager.cpp
 * Author: simon
 * 
 * Created on October 24, 2014, 2:28 AM
 */

#include <openssl/obj_mac.h>
#include <sched.h>

#include <cstring>
#include <random>
#include <iostream>
#include <fstream>
#include <iomanip>


#include "BroadcastSecurityManager.hh"
#include "network_handler.h"


static void *g_run(void *);
static void *a_run(void *);

BroadcastSecurityManager::BroadcastSecurityManager(uint32_t frame_len, struct fbp_header* header, Encoder* encoder, unsigned char **cw_data, bool *interrupt) : bufferSize(N_OF_HASHES * N_PHASES) {
	//keypair = RSA_generate_key(RSA_KLEN, PUB_EXP, nullptr, nullptr); // Deprecated, replaced by RSA_generate_key_ex
	BIGNUM *e;
	e = BN_new();
	BN_set_word(e, PUB_EXP);
	keypair = RSA_new();
	RSA_generate_key_ex(keypair, RSA_KLEN, e, NULL);
	BN_free(e);
	e = NULL;
	rIdx = 0;
	wIdx = 0;
	aIdx = 0;
	maxIdx = 0;
	prev_maxIdx = maxIdx;
	lseq = N_OF_HASHES-1;
	D = N_OF_HASHES;
#if OPENSSL_VERSION_NUMBER < 0x10100000L
	for (int i = 0; i < bufferSize; i++)
	{
		BN_bn2bin(keypair->n, authenticator[i].pkey);
		authenticator[i].code = 0;
	}
#else
	const BIGNUM *n;
	RSA_get0_key(keypair, &n, NULL, NULL);
	for (int i = 0; i < bufferSize; i++)
	{
		BN_bn2bin(n, authenticator[i].pkey);
		authenticator[i].code = 0;
	}
#endif
	rsaLen = RSA_KLEN / 8;
	dataLen = sizeof (struct BroadcastAuthenticator) -rsaLen * 2;
	for (int i = 0; i < bufferSize; i++)
		frame[i] = new uint8_t[frame_len];
	for (int i = 0; i < bufferSize; i++)
		auth[i] = false;
	r_interval = N_OF_HASHES-1;
	sem_init(&gsem, 0, 4);
	sem_init(&asem, 0, 0);
	sem_init(&ssem, 0, 0);
	cpu_set_t my_set;		/* Define your cpu_set bit mask. */
	CPU_ZERO (&my_set);		/* Initialize it all to 0, i.e. no CPUs selected. */
	CPU_SET (0, &my_set);	/* set the bit that represents core 0. */
	sched_setaffinity (0, sizeof (cpu_set_t), &my_set);
	g_args.header = header;
	g_args.encoder = encoder;
	g_args.cw_data = cw_data;
	g_args.frame_len = frame_len;
	g_args.bsm = (void *)this;
	a_args.frame_len = frame_len;
	a_args.bsm = (void *)this;
	n_of_hashes = N_OF_HASHES;
}

BroadcastSecurityManager::~BroadcastSecurityManager() {
	for (int i = 0; i < bufferSize; i++) delete[] frame[i];
	RSA_free(keypair);
}

void BroadcastSecurityManager::startGen() {
	pthread_create(&gthread, NULL, g_run, (void *)&g_args);
	pthread_detach(gthread);
}

void BroadcastSecurityManager::startAuth() {
	pthread_create(&athread, NULL, a_run, (void *)&a_args);
	pthread_detach(athread);
}

void BroadcastSecurityManager::writeFrame(uint8_t *frame, uint32_t frame_len) {
	if ((wIdx % n_of_hashes) == 0)
		sem_wait(&gsem);
	memcpy(this->frame[wIdx], frame, frame_len);
	lseq++;
	authenticator[wIdx].lseq = lseq;
	wIdx = (wIdx + 1) % bufferSize;
	if ((wIdx % n_of_hashes == 0) && (lseq >= (n_of_hashes<<2)))
		sem_post(&asem);
}

uint8_t* BroadcastSecurityManager::readFrame() {
	uint8_t *frame = this->frame[rIdx];
	rIdx = (rIdx + 1) % bufferSize;
	if (rIdx % n_of_hashes == 0)
		sem_post(&gsem);
	return frame;
}

void BroadcastSecurityManager::signFrames(uint32_t frame_len) {
	if ((aIdx % n_of_hashes) == 0)
		sem_wait(&asem);
	if (aIdx == maxIdx) {
		auth[aIdx] = true;
		memcpy(&authenticator[aIdx].hash[0], &authenticator[prev_maxIdx].hash[D], (n_of_hashes - D) * SHA_DIGEST_LENGTH);
		for (int i = n_of_hashes - D; i < n_of_hashes; i++)
			SHA1(frame[(maxIdx + i) % bufferSize], frame_len, authenticator[aIdx].hash[i]);
		prev_maxIdx = maxIdx;
		maxIdx = (maxIdx + (D = randRange())) % bufferSize;
		uint8_t digest [SHA256_DIGEST_LENGTH];
		SHA256((uint8_t *) & authenticator[aIdx], dataLen, digest);
		uint32_t slen;
		RSA_sign(NID_sha256, digest, sizeof (digest), authenticator[aIdx].sign, &slen, keypair);
	}
	aIdx = (aIdx + 1) % bufferSize;
	if (aIdx % n_of_hashes == 0) {
	D = n_of_hashes;
	sem_post(&ssem);
	}
}

uint8_t* BroadcastSecurityManager::getAuthenticator() {
	auth[rIdx] = false;
	return (uint8_t *) &authenticator[rIdx];
}

void BroadcastSecurityManager::displayPublickeyHash() {
	uint8_t digest [SHA_DIGEST_LENGTH];
	SHA1(authenticator[0].pkey, rsaLen, digest);
	cout << "Public Key SHA1: ";
	for (int i = 0; i < SHA_DIGEST_LENGTH; i++) cout << uppercase << hex << (int) digest[i];
	cout << endl;
}

void BroadcastSecurityManager::storePublickeyHash(char *keyfile) {
	uint8_t digest [SHA_DIGEST_LENGTH];
	ofstream file;
	SHA1(authenticator[0].pkey, rsaLen, digest);
	file.open (keyfile);
	for (int i = 0; i < SHA_DIGEST_LENGTH; i++) file << uppercase << hex << (int) digest[i];
	file << endl;
	file.close();
}

bool BroadcastSecurityManager::authenticatorAvailable() {
	if ((rIdx % n_of_hashes) == 0)
		sem_wait(&ssem);
	return auth[rIdx];
}

int BroadcastSecurityManager::randRange() {
	return 1 + rand() % r_interval;
}

void BroadcastSecurityManager::setRInterval(uint32_t r_interval) {
	this->r_interval = r_interval;
}

static void *g_run(void *args) {
	struct fbp_header* header = ((gthr_args *)args)->header;
	Encoder* encoder = ((gthr_args *)args)->encoder;
	unsigned char **cw_data = ((gthr_args *)args)->cw_data;
	int frame_len = ((gthr_args *)args)->frame_len;
	BroadcastSecurityManager *bsm = (BroadcastSecurityManager *)((gthr_args *)args)->bsm;
	cpu_set_t my_set;        /* Define your cpu_set bit mask. */
	CPU_ZERO(&my_set);       /* Initialize it all to 0, i.e. no CPUs selected. */
	CPU_SET(1, &my_set);     /* set the bit that represents core 1. */
	sched_setaffinity(0, sizeof(cpu_set_t), &my_set);
	for(;;) {
		(header->seq)++;
		encoder->genCodeword(&header->seed, &header->degree, cw_data);
		bsm->writeFrame((uint8_t *) header, frame_len);
	}
	return nullptr;
}

static void *a_run(void *args) {
	int frame_len = ((athr_args *)args)->frame_len;
	BroadcastSecurityManager *bsm = (BroadcastSecurityManager *)((athr_args *)args)->bsm;
	cpu_set_t my_set;        /* Define your cpu_set bit mask. */
	CPU_ZERO(&my_set);       /* Initialize it all to 0, i.e. no CPUs selected. */
	CPU_SET(2, &my_set);     /* set the bit that represents core 2. */
	sched_setaffinity(0, sizeof(cpu_set_t), &my_set);
	for(;;) bsm->signFrames(frame_len);
	return nullptr;
}
