/* 
 * File:   Symbol.cpp
 * Author: joao
 * 
 * Created on April 9, 2013, 6:35 PM
 */

#include <stdlib.h>
#include <stdio.h>
#include "Symbol.hh"
#include "Codeword.hh"

Symbol::Symbol() {
    decoded_word = NULL;
    //codewords = new list<Codeword*>();
}

Symbol::~Symbol() {
    Codeword* c;
    while (!codewords.empty()) {
        // remove all dependencies
        c = codewords.back();
        codewords.pop_back();
        if (c->removeDependency(NULL) < 1) delete c;
    }
    if (decoded_word != NULL) delete decoded_word;
}

bool Symbol::isDecoded() {
    return decoded_word != NULL;
}

/*u_int8_t Symbol::addCodeword(Codeword* cw, queue<Codeword*>* new_symbols) {
    Codeword* c;
    list<Codeword*>::iterator it;

    if (DEBUG) printf("addCodeword2()\n");

    // symbol already decoded. remove dependency
    if (isDecoded()) {
        cw->removeDependency(decoded_word);
        return 0;
    }

    // symbol and codeword not decoded. put codeword in a dependencies list
    if (!cw->isSymbol()) {
        codewords.push_front(cw);
        return 1;
    }

    // codeword is a symbol. resolve dependendies and set symbol as decoded
    while (!codewords.empty()) {
        // for each dependency
        c = codewords.back();
        if (c->removeDependency(cw) == 1) { // new symbol
            new_symbols->push(c);
            if (DEBUG) printf("*");
        }
        codewords.pop_back();
    }
    decoded_word = cw;

    return 2;
}*/

void Symbol::addCodeword(Codeword* cw) {
    // add codeword to this symbol list
    if (!cw->isSymbol()) codewords.push_front(cw);
}

bool Symbol::setDecodeword(Codeword* cw, queue<Codeword*>* new_symbols) {
    
    if (isDecoded()) return false;
    
    Codeword* c;
    while (!codewords.empty()) {
        // for each codeword
        c = codewords.back();
        if (c->removeDependency(cw) == 1) {
            // new symbol
            new_symbols->push(c);
        }
        codewords.pop_back();
    }
    
    decoded_word = cw;
    return true;
}

unsigned char* Symbol::getData() {
    if (isDecoded()) return decoded_word->getData();
    return NULL;
}

Codeword* Symbol::getDecodeword() {
    if (isDecoded()) return decoded_word;
    return NULL;
}

void Symbol::printSymbol() {
    printf(" %d ", isDecoded());
    if (isDecoded()) {
        decoded_word->printCodeword();
    } else {
        if (codewords.size() > 0) {
            list<Codeword*>::iterator it_cod;
            for (it_cod = codewords.begin(); it_cod != codewords.end(); it_cod++) {
                (*it_cod)->printCodeword();
            }
        }
    }
}
