/* 
 * File:   Logger.cpp
 * Author: joao
 * 
 * Created on April 19, 2013, 12:39 AM
 */

#include "Logger.hh"

#include <fcntl.h>

Logger::Logger(const char* filename, const char* modes) {
    fp = 0;

    if (!(fp = fopen(filename, modes))) {
        fprintf(stderr, "Error opening log file %s for writting\n", filename);
        exit(1);
    }
}

Logger::~Logger() {
    if (fp != NULL) fclose(fp);
}

void Logger::append( const char *msg, ... )
{
	if ( !fp ) return;

	va_list ap;    
	va_start( ap, msg );
	vfprintf( fp, msg, ap );
	fflush( fp );
	va_end( ap );
}

