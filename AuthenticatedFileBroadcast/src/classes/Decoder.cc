/* 
 * File:   Decoder.cpp
 * Author: joao
 * 
 * Created on April 8, 2013, 11:49 PM
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Decoder.hh"
#include "Logger.hh"
#include "lt_handler.h"

#include <random>
#include <iostream>
#include <algorithm>

using namespace std;

Decoder::Decoder(unsigned char* data, u_int32_t k, off_t cw_size) {
    this->data = data;
    //this->data_size = data_size;
    this->cw_size = cw_size;
    this->k = k;
    //this->k = (unsigned long) data_size / chunk_size + (data_size % chunk_size > 0);
    this->cw_decoded = 0;
    this->cw_total = 0;
    this->cw_mem = 0;

    this->symbols = new Symbol[k];
    this->rp = new RandPerm(k);

    this->decodeThread = 0;
    pthread_mutex_init(&decodeMutex, NULL);
    pthread_cond_init(&decodeCondition, NULL);
}

Decoder::Decoder(u_int32_t k, off_t cw_size) {
    this->cw_size = cw_size;
    this->k = k;

    this->cw_decoded = 0;
    this->cw_total = 0;
    this->cw_mem = 0;

    this->symbols = new Symbol[k];
    this->rp = new RandPerm(k);

    this->decodeThread = 0;
    pthread_mutex_init(&decodeMutex, NULL);
    pthread_cond_init(&decodeCondition, NULL);
}

Decoder::~Decoder() {
    Codeword* c;
    end = true;
    pthread_mutex_lock(&decodeMutex);
    while (!recv_queue.empty()) {
        c = recv_queue.front();
        recv_queue.pop();
        delete c;
    }
    pthread_mutex_unlock(&decodeMutex);
    delete[] symbols;
    delete rp;
    if (decodeThread != 0) pthread_cancel(decodeThread);
    pthread_mutex_destroy(&decodeMutex);
    pthread_cond_destroy(&decodeCondition);
}

void* Decoder::threadHandler(void* pthis) {
    Decoder *ptr = static_cast<Decoder *> (pthis);
    ptr->decodeCycle();
    return NULL;
}

void Decoder::start() {
//  cout << "Start decoding... ";
    if (decodeThread != 0) pthread_cancel(decodeThread);
    end = false;
    pthread_create(&decodeThread, NULL, threadHandler, this);
//  cout << " OK" << endl;
}

void Decoder::stop() {
//  cout << endl << "Stop decoding...";
    fflush(stdout);

    end = true;

    pthread_mutex_lock(&decodeMutex);
    pthread_cond_signal(&decodeCondition);
    pthread_mutex_unlock(&decodeMutex);

    pthread_join(decodeThread, NULL);
//  cout << " OK" << endl;
}

void Decoder::decodeCycle() {
    Codeword* c;
    Logger log("/tmp/fb-decoder.log", "w");

    while (!isComplete() && !end) {
        pthread_mutex_lock(&decodeMutex);
        if (recv_queue.empty()) {
            // sleep if receive queue is empty
            pthread_cond_wait(&decodeCondition, &decodeMutex);
            pthread_mutex_unlock(&decodeMutex);
        } else {
            // get codeword from receive queue
            c = recv_queue.front();
            recv_queue.pop();
            pthread_mutex_unlock(&decodeMutex);

            // process codeword
            insertCodeword(c);
            log.append("k=%u; cw=%u; dw=%u; mem=%u;\n", k, cw_total, cw_decoded, cw_mem);
        }
        //if (!end && !isComplete()) insertCodeword2(c); // process codeword
        //else delete c; // delete codeword
    }
    pthread_exit(NULL);
}

bool Decoder::isComplete() {
    return cw_decoded >= k;
}

int Decoder::addCodeword(u_int32_t cw_seed, u_int32_t cw_degree, unsigned char* cw_data) {
    // Create the codeword and notify decoding thread
    pthread_mutex_lock(&decodeMutex);
    recv_queue.push(new Codeword(cw_seed, cw_degree, cw_data, cw_size));
    pthread_cond_signal(&decodeCondition);
    pthread_mutex_unlock(&decodeMutex);

    return 0;
}

u_int32_t Decoder::getK() {
    return k;
}

u_int32_t Decoder::getTotalCW() {
    return cw_total;
}

u_int32_t Decoder::getDecodedCW() {
    return cw_decoded;
}

void Decoder::writeData() {
    unsigned int i;
    unsigned char* ptr_data = data;
    for (i = 0; i < k; i++) {
        if (symbols[i].isDecoded())
            memcpy(ptr_data, symbols[i].getData(), cw_size);
        ptr_data += cw_size;
    }
}

void Decoder::writeData(unsigned char* fdata) {
    unsigned int i;
    unsigned char* ptr_data = fdata;
    for (i = 0; i < k; i++) {
        if (symbols[i].isDecoded())
            memcpy(ptr_data, symbols[i].getData(), cw_size);
        ptr_data += cw_size;
    }
}

void Decoder::printData() {
    u_int32_t i;
    printf("printData()\n");
    printf("Number Decoded Symbols -> %u\n", cw_decoded);
    printf("Symbols: \n");
    for (i = 0; i < k; i++) {
        printf("[%u]:", i);
        symbols[i].printSymbol();
        printf(" \t [%p]", symbols[i].getDecodeword());
        printf("\n");
    }
}

void Decoder::printProgress(unsigned int recv, unsigned int total) {
    double loss_p = (double) recv / (double) total;
    loss_p = 1.0 - loss_p;
    loss_p *= 100.0;
#ifdef DEBUG
    printf("\rDownload progress: %u/%u/%u (%u%%); mem -> %u ; loss -> %2.2f%%", cw_decoded, cw_total, k, 100 * cw_decoded / k, cw_mem, loss_p);
#else
    printf("\rDownload progress: %u%%  ", min((unsigned int) 100, (unsigned int) (100 * cw_total / k)));
#endif
    fflush(stdout);
}

/*int Decoder::insertCodeword(Codeword* cw) {
    u_int8_t result = 0;
    u_int32_t cw_degree;
    u_int32_t index;
    queue<Codeword*> new_symbols;
    Codeword* sym;

    if (DEBUG) {
        printf("insertCodeword()\n");
        printf("index:");
    }

    cw_total++;
    cw_mem++;
    allCW.push_back(cw);

    // set seed and degree
    rp->setSeed(cw->getSeed());
    cw_degree = cw->getDegree();
    for (u_int32_t i = 0; i < cw_degree; i++) {
        // for each symbol in codeword
        index = rp->getPerm();
        // add index to codeword
        cw->addDepIndex(index);
        // add codeword to symbols list
        result |= symbols[index].addCodeword(cw, &new_symbols);
    }

    if (result == 0) {
        delete cw;
        cw_mem--;
        allCW.remove(cw);
    } else if ((result & 2) == 2) cw_decoded++;

    // while have new symbols
    while (!new_symbols.empty()) {
        sym = new_symbols.front(); // get top symbol/codeword
        new_symbols.pop(); // remove top symbol/codeword

        // add codeword to a symbol or delete if symbol already decoded
        if (symbols[sym->getIndex()].addCodeword(sym, &new_symbols)) cw_decoded++;
        else {
            delete sym;
            cw_mem--;
            allCW.remove(sym);
        }
    }

    if (DEBUG) printf("\n");
    return 0;
}*/

int Decoder::insertCodeword(Codeword* cw) {
    bool added = false;
    u_int32_t cw_degree;
    u_int32_t index;
    queue<Codeword*> new_symbols;
    Codeword* sym;

    // update codeword counters 
    cw_total++;
    cw_mem++;

    // set seed and degree
    rp->setSeed(cw->getSeed());
    cw_degree = cw->getDegree();

    // add codeword to depend symbols list
    for (u_int32_t i = 0; i < cw_degree; i++) {
        // for each depend symbol of codeword
        index = rp->getPerm();

        if (symbols[index].isDecoded()) {
            // symbol is decoded
            XOR_u64(cw->getData(), symbols[index].getData(), cw->getData(), cw_size); // XOR data
            cw->decrementDegree(); // degree--
        } else {
            //symbol is not decoded
            cw->addDepIndex(index); // add index to codeword
            symbols[index].addCodeword(cw); // add Codewords to symbol list
            added = true;
        }
    }

    if (!added) {
        delete cw;
        cw_mem--;
    }
    else if (cw->isSymbol()) new_symbols.push(cw);

    // while have new symbols
    while (!new_symbols.empty()) {
        sym = new_symbols.front(); // get top symbol/codeword
        new_symbols.pop(); // remove top symbol/codeword

        // set symbol has decoded
        if (!symbols[sym->getIndex()].setDecodeword(sym, &new_symbols)) {
            // unecessary symbol
            delete sym;
            cw_mem--;
        } else cw_decoded++;
    }
    return 0;
}
