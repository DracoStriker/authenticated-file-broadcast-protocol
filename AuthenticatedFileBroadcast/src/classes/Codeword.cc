/* 
 * File:   Codeword.cpp
 * Author: joao
 * 
 * Created on April 9, 2013, 3:48 PM
 */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "Codeword.hh"
#include "RandPerm.hh"
#include "lt_handler.h"

Codeword::Codeword(u_int32_t cw_seed, u_int32_t cw_degree, u_int32_t cw_index, unsigned char* cw_data, off_t cw_data_size) {
    //count++;
    
    this->seed = cw_seed;
    this->degree = cw_degree;
    this->data_size = cw_data_size;
    this->index = cw_index;

    // copy data content
    data = (unsigned char*) malloc(data_size);
    memcpy(data, cw_data, data_size);
}

Codeword::Codeword(u_int32_t cw_seed, u_int32_t cw_degree, unsigned char* cw_data, off_t cw_data_size) {
    //count++;
    
    this->seed = cw_seed;
    this->degree = cw_degree;
    this->data_size = cw_data_size;
    this->index = 0;

    // copy data content
    data = (unsigned char*) malloc(data_size);
    memcpy(data, cw_data, data_size);
}

Codeword::~Codeword() {
//  count--;
    
//  printf("\n~Codeword(%u)\n", index);
    free(data);
}

void Codeword::addDepIndex(u_int32_t index) {
    this->index ^=index;
}

int Codeword::removeDependency(Codeword* sym) {

    if (sym == NULL)  return --degree;
    
    if (degree > 1) {
        
        index ^= sym->getIndex();
        XOR_u64(data, sym->getData(), data, data_size);
        
        return --degree;
    }

    return -1;
}

bool Codeword::isSymbol() {
    return degree == 1;
}

u_int32_t Codeword::getDegree() {
    return degree;
}

u_int32_t Codeword::getIndex() {
    return index;
}

u_int32_t Codeword::getSeed() {
    return seed;
}

unsigned char* Codeword::getData() {
    return data;
}

void Codeword::printCodeword() {
    /*if (degree == 1)
        printf(" (%u;%u;%s)", index, degree, data);
    else*/
        printf(" (%u;%u)", index, degree);
}

u_int32_t Codeword::decrementDegree() {
    if (degree > 0) degree--;
    return degree;
}
