#!/bin/sh

EXPECTED_ARGS=2
TOTAL_ARGS=3
E_BADARGS=65

if [ $# -lt $EXPECTED_ARGS ]
then
  echo "Usage: `basename $0` <interface> <channel> [bssid_file]"
  exit $E_BADARGS
fi

WLAN=$1
CHANNEL=$(( $2 * 5 ))
CHANNEL=$(( $CHANNEL + 2407 ))
BSSID=$(cat /dev/urandom | tr -cd 'A-F0-9' | head -c 1)$(cat /dev/urandom | tr -cd 'AE26' | head -c 1)$(cat /dev/urandom | tr -cd 'A-F0-9' | head -c 10)
BSSID=$(echo $BSSID | cut -c 1-2):$(echo $BSSID | cut -c 3-4):$(echo $BSSID | cut -c 5-6):$(echo $BSSID | cut -c 7-8):$(echo $BSSID | cut -c 9-10):$(echo $BSSID | cut -c 11-12)

if [ $# -eq $TOTAL_ARGS ]
then
	echo $BSSID > $3
fi

SSID=$BSSID

set -x

ifconfig $WLAN down
iw $WLAN set type ibss
ifconfig $WLAN up
iw $WLAN ibss leave
iw $WLAN ibss join $SSID $CHANNEL $BSSID basic-rates 54 mcast-rate 54

set +x

echo "BSSID:   $BSSID"
echo "CHANNEL: $2"
echo "SSID:    $SSID"
